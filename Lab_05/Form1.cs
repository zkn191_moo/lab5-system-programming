﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InterfaceLibrary;

namespace Lab_05
{
    public partial class Form1 : Form
    {
        private string functionsDllPAth = $@"{Environment.CurrentDirectory}\CalculationsLibrary.dll";
        private Type classType;
        private object functionClass;

        public Form1()
        {
            InitializeComponent();

            try
            {
                string message = Interface.Connected;
                MessageBox.Show(message);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
        }

        private void buttonLoadDll_Click(object sender, EventArgs e)
        {
            if (File.Exists(functionsDllPAth))
            {  
                Assembly DLL = Assembly.LoadFrom(functionsDllPAth);
                classType = DLL.GetType("CalculationsLibrary.Calculations");
                if (classType != null)
                {
                    try
                    {
                        functionClass = Activator.CreateInstance(classType);
                        MessageBox.Show("Підключено CalculationsLibrary.dll.");
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.ToString());
                        throw;
                    }
                }
            }
        }

        private void buttonStartMultiThreading_Click(object sender, EventArgs e)
        {
            if (functionClass != null)
            {
                classType.InvokeMember("Threads", BindingFlags.InvokeMethod, null, classType, new object[0]);
            }
            else
            {
                MessageBox.Show("Потрібно підключити бібліотеку.");
            }
        }

        private void buttonCheck_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"Math test: sin(0) = {Math.Sin(0)}");
        }
    }
}
