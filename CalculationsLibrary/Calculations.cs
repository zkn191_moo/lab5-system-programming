﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CalculationsLibrary
{
    public class Calculations
    {
        public static void Threads()
        {
            List<Thread> threads = new List<Thread>();
            var threadDelegate = new ThreadStart(SortArr);

            for (int i = 0; i < 10000; i++)
            {
                Thread thread = new Thread(threadDelegate) { IsBackground = true };
                thread.Start();
            }
        }

        private static int[] CreateArr()
        {
            var array = new int[10000000];
            var rand = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.Next(0, 1000);
            }
            return array;
        }
        private static void SortArr()
        {
            var arr = CreateArr();
            Array.Sort(arr);
        }
    }
}
